import sys
import os
import configparser
import numpy as np
from pathlib import Path
from scipy.interpolate import interp1d
from pkrh.io import rsf, CFGParser
from pkrh import transform
from pkrh.io import su,Logger
from pkrh.io import progress_bar
from pkrh.io.interpolation import SU_handler,get_shot_weight

try:
    fcfg=sys.argv[1]
except:
    fcfg='par_interpolation.ini'

logger=Logger("log_interpolation.txt","intpl 2d")
logger.print("reading %s"%fcfg)
cfg=CFGParser(fcfg)
pdict=cfg.parse_sections('Input data','Transformation','Output')
Path(pdict['intpldir']).mkdir(exist_ok=True)

fsu = pdict['su'] # fldr-sorted SU file
h_meter = pdict['gridsize'] # in meter

if pdict['time']:
    logger.print("output Time: %s"%pdict['time'])
if pdict['frequency']:
    logger.print("output Frequency: %s"%pdict['frequency'])
if pdict['laplace']:
    logger.print("output Laplace: %s"%pdict['laplace'])
    damps = rsf.fromfile(pdict['laplace_coeff_file'],'data')
    logger.print("    damping constants = %s"%damps)
if pdict['laplace_fourier']:
    logger.print("output Laplace-Fourier: %s"%pdict['laplace_fourier'])

out_par = configparser.ConfigParser()
sh=SU_handler(fsu,logger)

# Get x-range
if pdict['auto_coordinate']:
    xmin = sh.sx_list.min()
    xmax = sh.sx_list.max()
    if pdict['include_receiver']:
        xmin = min(xmin, sh.gx_list.min())
        xmax = max(xmax, sh.gx_list.max())
    # padding
    pad_left=pdict['pad_left']
    pad_right=pdict['pad_right']
    xmin -= pad_left
    xmax += pad_right
else:
    xmin = pdict['xmin']
    xmax = pdict['xmax']

nx = int(np.ceil((xmax-xmin)/h_meter + 1))
if pdict.get('set_nx',None):
    logger.print("orignal nx = %d"%nx)
    nx = pdict['set_nx']
    logger.print("set nx = %d"%nx)
ax_intp= np.arange(nx)*h_meter + xmin


# Output info
logger.print("nx=%d, h_meter=%s, xmin=%s, xmax=%s"%(nx,h_meter,xmin,xmax))
out_par['Interpolation']={
        'h_meter':h_meter,
        'xmin':xmin,
        'xmax':xmax,
        'nx':nx}
out_par['Output']={
        'shot_weight_file':os.path.abspath(pdict['shot_weight_file']),
        'shot_file':os.path.abspath(pdict['shot_file']),
        'swdep_file':os.path.abspath(pdict['swdep_file'])}


# Shot weight
logger.print("calculating shot weight")
shot_weight=np.zeros((sh.nshot,4),dtype=np.float32)
for ishot,sx in enumerate(sh.sx_list):
    shot_weight[ishot,:]=get_shot_weight(sx,h_meter,nx,xmin)[:]

with rsf.output(pdict['shot_weight_file'],ax1=rsf.Axis(n=4,label="x1,x2,w1,w2"),ax2=rsf.Axis(n=sh.nshot,o=1,d=1,label="Shot Number"),data_format='ascii_float') as so:
    so.write(shot_weight)

# Shot coordinate
shot_coord = np.zeros((sh.nshot,2),dtype=np.float32)
shot_coord[:,0] = sh.sx_list - xmin
shot_coord[:,1] = sh.sy_list
with rsf.output(pdict['shot_file'],ax1=rsf.Axis(n=2,label="x,y"),ax2=rsf.Axis(n=sh.nshot,o=1,d=1,label="Shot Number"),data_format='ascii_float') as so:
    so.write(shot_coord)


# Sea water depth
logger.print("writing swd")
fsw = interp1d(x=sh.sx_list,y=sh.swd,kind='linear',bounds_error=False,fill_value=(sh.swd[0],sh.swd[-1]))
swd_nx = fsw(ax_intp)

with rsf.output(pdict['swdep_file'],ax1=rsf.Axis(n=nx,o=xmin,d=h_meter,label="X"),data_format='ascii_float') as so:
    so.write(swd_nx)


# Tmax cut
tmax = sh.nt*sh.dt
tmax_cut = pdict['tmax_cut']
if tmax_cut < tmax:
    nt = int(np.around(tmax_cut/sh.dt))
    tmax = nt*sh.dt
    logger.print("tmax cut = %s, nt_org = %d, nt_cut = %d"%(tmax_cut,sh.nt,nt))
else:
    nt = sh.nt

at=np.arange(nt)*sh.dt


# Output info
if pdict['time']:
    out_par['Output']['time_file']=os.path.abspath(pdict['time_file'])
    out_par['Time']={
            'nt':nt,
            'dt':sh.dt}

if pdict['frequency']:
    fmax=pdict['fmax']
    df = 1.0/tmax
    nf = int(fmax/df+1)
    logger.print("Fourier Transform: fmax=%s, nf=%d, df=%s"%(fmax,nf,df))
    out_par['Output']['frequency_file']=os.path.abspath(pdict['frequency_file'])
    out_par['Frequency']={
            'fmax':fmax,
            'df':df,
            'nf':nf}

if pdict['laplace']:
    logger.print("Laplace Transform")
    lt_table = transform.gen_lt_table(at,damps,sh.dt)
    out_par['Output']['laplace_file']=os.path.abspath(pdict['laplace_file'])
    out_par['Laplace']={'laplace_coeff_file':os.path.abspath(pdict['laplace_coeff_file'])}

if pdict['laplace_fourier']:
    # NOT IMPLEMENTED YET
    logger.print("Laplace-Fourier Transform")
    out_par['Output']['laplace_fourier_file']=os.path.abspath(pdict['laplace_fourier_file'])
    out_par['Laplace-Fourier']={}

with open(pdict['fout_intpl'],'w') as cfg:
    out_par.write(cfg)


# space-interpolation (space-time domain)
for ishot in range(sh.nshot):
    #logger.print("ishot=%d"%(ishot))
    progress_bar(ishot,sh.nshot-1," ishot=%d, nshot=%d"%(ishot,sh.nshot))
    # for freq, lapl, lapl-fourier
    if ishot == 0:
        mode='wb'
    else:
        mode='ab'
    # get shot
    seismo=sh.get_shot(ishot).T[:nt,:] # shape=(nt,nx) for interpolation over x axix
    ax = sh.get_receiver_coordinate(ishot)

    # interpolation
    fsm = interp1d(x=ax,y=seismo,axis=-1,kind='linear',bounds_error=False,fill_value=0.)
    seismo_intp=fsm(ax_intp).astype(np.float32).T # shape=(nx,nt) for output and fft

    # Time-domain shot gather
    if pdict['time']:
        seismo_intp.tofile("%s%04d"%(pdict['time_file'],ishot))


    # Fourier transform
    if pdict['frequency']:
        seismo_fft = np.fft.fft(seismo_intp,axis=-1).astype(np.complex64)
        #seismo_fft[:,:nf].tofile("%s%04d"%(pdict['frequency'],ishot))
        for ifreq in range(nf):
            with open("%s%03d"%(pdict['frequency_file'],ifreq),mode) as ff:
                ff.seek(8*nx*ishot)
                seismo_fft[:,ifreq].tofile(ff)

        # Inverse Fourier Transform
        if pdict['check_ift_skip'] > 0:
            if ishot % pdict['check_ift_skip'] == 0:
                #logger.print("IFT ishot=%d"%(ishot))
                seismo_fft[:,nf:-nf-1]=0.
                seismo_ift = np.fft.ifft(seismo_fft)
                seismo_ift.real.astype(np.float32).tofile("%s%04d"%(pdict['frequency_ift_file'],ishot))


    # Laplace transform
    if pdict['laplace']:
        for idamp,_ in enumerate(damps):
            with open("%s%03d"%(pdict['laplace_file'],idamp),mode) as fl:
                lt_seismo=transform.integral_transform(seismo_intp,lt_table[idamp])
                fl.seek(8*nx*ishot)
                lt_seismo.tofile(fl)


    # Laplace-Fourier transform
    # if pdict['laplace_fourier']:
    # NOT IMPLEMENTED YET

sh.close()
logger.print('')
logger.print("End")
