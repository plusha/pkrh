from pkrh.io import rsf
from pkrh.io.optparse import Par
from pkrh import wavelets

with Par("Source wavelet Generator") as par:
    nt=par.i('nt',msg="Number of time samples")
    fmax=par.f('fmax',msg="Cutoff frequency")
    dt=par.f('dt',default=0.001,msg="Sampling interval")
    wav=par.s('wav',default='fdgaus',msg="ricker | fdgaus")
    out=par.s('out',default='stdout',msg="Output rsf file name")
    form=par.s('form',default='ascii',msg='native | ascii')

if wav == "ricker":
    w=wavelets.ricker(fmax,dt,nt,dtype='f')
elif wav == "fdgaus":
    w=wavelets.fdgaus(fmax,dt,nt,dtype='f')

at=rsf.Axis(n=nt,d=dt,label='Time',unit='s')

with rsf.output(out,ax1=at,form=form) as fout:
    fout.write(w)
