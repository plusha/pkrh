import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("2D Shot file generator") as par:
    out=par.s('out',default='',msg='Output rsf file name')
    nshot=par.i('nshot',msg='Number of shots')
    sx0=par.f('sx0',msg='First shot x position (in km)')
    dsx=par.f('dsx',msg='Increments of shot x positions (in km)')
    sz=par.f('sz',msg='Depth of shots (in km)')
    fmt=par.s('fmt',default='%10.5f',msg='Output ascii format')

# 2d
shot=np.zeros((nshot,2),dtype='f')
for ishot in range(nshot):
    shot[ishot,0]=sx0+ishot*dsx
    shot[ishot,1]=sz

ax1=rsf.Axis(n=2,d=1,label="Dimension")
ax2=rsf.Axis(n=nshot,d=1,label="Shot number")

fout=rsf.output(out,ax1=ax1,ax2=ax2,form='ascii')
fout.write(shot,close=True,fmt=fmt)
