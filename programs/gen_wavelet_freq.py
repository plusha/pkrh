import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par
from pkrh import wavelets

with Par("Frequency-domain Source wavelet Generator") as par:
    nt=par.i('nt',msg="Number of time samples")
    fmax=par.f('fmax',msg="Cutoff frequency")
    dt=par.f('dt',default=0.001,msg="Sampling interval")
    s=par.f('s',default=0.0,msg="Attenuation: apply exp(-s*t)")
    wav=par.s('wav',default='fdgaus',msg="ricker | fdgaus")
    out=par.s('out',default='stdout',msg="Output rsf file name")

if wav == "ricker":
    w=wavelets.ricker(fmax,dt,nt,dtype='f')
elif wav == "fdgaus":
    w=wavelets.fdgaus(fmax,dt,nt,dtype='f')

# apply damping
at=np.arange(nt)*dt
damp=np.exp(-s*at)
w=w*damp
cw=np.fft.fft(w)

#at=rsf.Axis(n=nt,d=dt,label='Time',unit='s')
tmax=nt*dt
df=1.0/tmax
nf=int(round(fmax/df+1))
af=rsf.Axis(n=nf,d=df,o=0.,label='Frequency',unit='Hz')

with rsf.output(out,ax1=af,data_format='native_complex') as fout:
    fout.write(cw[:nf])
