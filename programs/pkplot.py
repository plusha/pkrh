import sys
import os
from subprocess import call
import matplotlib.pyplot as plt
import numpy as np
from pkrh.io import rsf

def run(cmd):
    print(cmd)
    call(cmd,shell=True)

def plot_1d(f,opt=''):
    datafile=f.get('in')
    run('gnuqp %s %s'%(opt,datafile))


def parse_axis(ax,idim):
    return "n%d=%d f%d=%s d%d=%s label%d=%s"%(idim,ax.n,idim,ax.o,idim,ax.d,idim,ax.label)

def plot_2d(f,opt=''):
    datafile=f.get('in')
    ax1,ax2=f.gets(["ax1","ax2"])
    cmd="ximage %s %s hbox=300 title=%s %s < %s"%(parse_axis(ax1,1),parse_axis(ax2,2),os.path.basename(datafile),opt,datafile)
    run(cmd)

if len(sys.argv) < 2:
    print("RSF plot\n%s [input rsf file: 1D/2D]"%sys.argv[0])
    print("Options: 1D gnuqp options, 2D ximage options")
    sys.exit(1)

fname=sys.argv[1]
if len(sys.argv)>2:
    opt=' '.join(sys.argv[2:])
else:
    opt=''

with rsf.input(fname) as f:
    ndim=f.dimension
    if ndim == 1:
        plot_1d(f,opt)
    elif ndim == 2:
        plot_2d(f,opt)

