import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("2D Laplace-Fourier domain damping constant file generator") as par:
    out=par.s('out',default='',msg='Output rsf file name')

    nf=par.i('nf',default=1,msg='Number of frequencies')
    f0=par.f('f0',default=0.,msg='First frequency (in Hz)')
    df=par.f('df',default=0.,msg='Increments of frequencies (in Hz)')

    ns=par.i('ns',default=1,msg='Number of damping constants')
    s0=par.f('s0',msg='First damping constant (in s^{-1})')
    ds=par.f('ds',default=0.,msg='Increments of dampings (in s^{-1})')

    nt=par.i('nt',default=0,msg='Number of time samples, if nt>0: calculate nf,df automatically using f0,dt,fmax')
    dt=par.f('dt',default=0.001,msg='Time sampling rate (in sec)')
    fmax=par.f('fmax',default=0.,msg='Maximum frequency (in Hz)')

if nt > 0:
    tmax=nt*dt
    df=1./tmax
    nf=int(round((fmax-f0)/df))
    print("nf=%s, df=%s",nf,df)

omega=np.zeros(nf*ns,dtype=np.complex64)
ii=0
for ifreq in range(nf):
    freq=f0+ifreq*df
    for idamp in range(ns):
        damp=s0+idamp*ds
        omega[ii]=2.*np.pi*freq - 1.0j * damp
        ii+=1

ax1=rsf.Axis(n=nf*ns,d=1,label="Complex frequency")

fout=rsf.output(out,ax1=ax1,data_format='native_complex')
fout.write(omega,close=True)
