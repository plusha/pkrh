import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("3D Shot file generator") as par:
    out=par.s('out',default='',msg='Output rsf file name')

    sx0=par.f('sx0',msg='First shot x position (in km)')
    dsx=par.f('dsx',msg='Increments of shot x positions (in km)')
    nsx=par.i('nsx',msg='Number of shots in x direction')

    sy0=par.f('sy0',msg='First shot y position (in km)')
    dsy=par.f('dsy',msg='Increments of shot y positions (in km)')
    nsy=par.i('nsy',msg='Number of shots in y direction')

    sz =par.f('sz',msg='Depth of shots (in km)')
    fmt=par.s('fmt',default='%10.5f',msg='Output ascii format')

# 3d
nshot = nsx * nsy
shot = np.zeros((nshot,3),dtype='f')

ishot=0
for isy in range(nsy):
    for isx in range(nsx):
        shot[ishot,0]=sx0+isx*dsx
        shot[ishot,1]=sy0+isy*dsy
        shot[ishot,2]=sz
        ishot+=1

ax1=rsf.Axis(n=3,d=1,label="Dimension")
ax2=rsf.Axis(n=nshot,d=1,label="Shot number")

fout=rsf.output(out,ax1=ax1,ax2=ax2,form='ascii')
fout.write(shot,close=True,fmt=fmt)
