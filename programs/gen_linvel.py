import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("2D V(z) velocity Generator") as par:
    n1=par.i('n1',msg="Number of grids in the fast dimension")
    n2=par.i('n2',msg="Number of grids in the slow dimension")
    h=par.f('h',msg="Grid size in km")
    vmin=par.f('vmin',msg="Minimum velocity at the top")
    vmax=par.f('vmax',msg="Maximum velocity at the bottom")
    vwater=par.f('vwater',default=1.5,msg="Water layer velocity")
    fswd=par.s('swd',default='',msg='Sea water depth rsf file [in meter]')
    tswd=par.i('tswd',default=1,msg='SWD type 1: land from vmin, 2: waterlayer cut linvel')
    out=par.s('out',default='stdout',msg="Output rsf file name")

a1=rsf.Axis(n=n1,d=h,label='Depth',unit='km')
a2=rsf.Axis(n=n2,d=h,label='Distance',unit='km')

vel=np.zeros((n2,n1),dtype=np.float32)

if fswd:
    print("Swdep file=",fswd)
    iswd=np.around(rsf.fromfile(fswd,'data')/1000./h).astype(np.int32)
    if tswd==1:
        print("swd type=1, land from vmin")
        for i2 in range(n2):
            vel[i2,:iswd[i2]]=vwater
            for i1 in range(iswd[i2],n1):
                vel[i2,i1]=vmin+(vmax-vmin)*(i1-iswd[i2])/(n1-iswd[i2])
    elif tswd==2:
        print("swd type=2, waterlayer cut linvel")
        for i1 in range(n1):
            vel[:,i1]=vmin+(vmax-vmin)*i1/(n1-1)
        for i2 in range(n2):
            vel[i2,:iswd[i2]]=vmin
else:
    for i1 in range(n1):
        vel[:,i1]=vmin+(vmax-vmin)*i1/(n1-1)

with rsf.output(out,ax1=a1,ax2=a2) as fout:
    fout.write(vel)
