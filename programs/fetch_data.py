import sys,subprocess,os
from pkrh.io import errexit

def run(cmd):
    subprocess.call(cmd,shell=True)


data={}
data['bp']="https://www.dropbox.com/s/1obce8ts1ln148i/bp.zip?dl=0"
data['marm']="https://www.dropbox.com/s/7qirh9b9jn8rtka/marm.zip?dl=0"
data['pluto15m']="https://www.dropbox.com/s/vpo4rthrr383yic/pluto15m.zip?dl=0"
data['pluto30m']="https://www.dropbox.com/s/g1alc0igk50av6p/pluto30m.zip?dl=0"
data['salt10m']="https://www.dropbox.com/s/a26cn0l2rgor88f/salt10m.zip?dl=0"
data['salt20m']="https://www.dropbox.com/s/leqc17sxz16ujly/salt20m.zip?dl=0"
data['salt40m']="https://www.dropbox.com/s/zps6r0aswomxeaf/salt40m.zip?dl=0"
data['yoon']="https://www.dropbox.com/s/p2llt3f758m43lh/yoon.zip?dl=0"

if len(sys.argv) < 2:
    print("""
    Download data from web

    Available velocity models:
        %s
    """%', '.join(sorted(list(data.keys()))))
    sys.exit(0)


try:
    name=sys.argv[1]
    link=data[name].replace('dl=0','dl=1')
except:
    errexit("Wrong name")
output=os.path.basename(link).split('?')[0]

if sys.platform == 'darwin':
    download="curl -L '%s' -o %s"%(link,output)
elif sys.platform == 'linux':
    download="wget %s -O %s"%(link,output)
else:
    errexit("OS not supported: %s"%sys.platform)

run(download)
run("unzip %s"%output)
run("rm %s"%output)
