import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("2D Laplace-Fourier domain damping constant file generator") as par:
    out=par.s('out',default='',msg='Output rsf file name')

    nfreq=par.i('nfreq',default=1,msg='Number of frequencies')
    f0=par.f('f0',default=0.,msg='First frequency (in Hz)')
    df=par.f('df',default=0.,msg='Increments of frequencies (in Hz)')

    ns=par.i('ns',msg='Number of damping constants')
    s0=par.f('s0',msg='First damping constant (in Hz)')
    ds=par.f('ds',msg='Increments of dampings (in Hz)')

    fmt=par.s('fmt',default='%10.5f',msg='Output ascii format')

alpha=np.zeros((nfreq*ns,2),dtype=np.float32)
ii=0
for ifreq in range(nfreq):
    freq=f0+ifreq*df
    for idamp in range(ns):
        damp=s0+idamp*ds
        alpha[ii,0]=2.*np.pi*freq
        alpha[ii,1]=-1.0*damp
        ii+=1

ax1=rsf.Axis(n=nfreq*ns,d=1,label="Complex frequency")
ax2=rsf.Axis(n=2,d=1)

fout=rsf.output(out,ax1=ax1,ax2=ax2,form='ascii')
fout.write(alpha,close=True,fmt=fmt)
