import os.path
import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("RSF absolute path") as par:
    inp=par.s('in',default='',msg='Input rsf file name')
    out=par.s('out',default='',msg='Output rsf file name')

fin=rsf.input(inp)
ax_dic=fin.axes_dict()

fout=rsf.output(data_format=fin.data_format,esize=fin.esize,**ax_dic,_in=os.path.abspath(fin.get('in')),open_in=False)
fout.write_rsf()

fin.close()
fout.close()
