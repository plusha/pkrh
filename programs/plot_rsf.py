import sys
from subprocess import call
import matplotlib.pyplot as plt
import numpy as np
from pkrh.io import rsf

def plot_1d(f,spec=False):
    data=f.read()
    ax=f.get("ax1")
    x=ax.o+np.arange(ax.n)*ax.d
    if spec:
        plt.subplot(211)

    # main
    plt.plot(x,data)
    plt.xlabel("%s [%s]"%(ax.label,ax.unit))

    if spec: # spectrum
        tmax=ax.n*ax.d
        df=1./tmax
        nf=int(ax.n/2)
        af=np.arange(nf)*df
        cw=np.fft.fft(data)
        plt.subplot(212)
        plt.plot(af,np.abs(cw[:nf]))
        plt.xlabel("Frequency [Hz]")
        plt.tight_layout()

    plt.show()

def plot_2d(f,transp=False,xflip=None,yflip=True,cmap='jet_r'):
    data=f.read(read_all=True).T
    if transp:
        data=data.T
    ax1,ax2=f.gets(["ax1","ax2"])
    xmin,xmax=ax2.o,ax2.o+(ax2.n-1)*ax2.d
    ymin,ymax=ax1.o,ax1.o+(ax1.n-1)*ax1.d
    extent=[xmin,xmax,ymin,ymax]
    if xflip:
        extent[0],extent[1]=extent[1],extent[0]
    if yflip:
        extent[2],extent[3]=extent[3],extent[2]
    plt.imshow(data,extent=extent,aspect='auto',cmap=cmap)
    plt.xlabel("%s [%s]"%(ax2.label,ax2.unit))
    plt.ylabel("%s [%s]"%(ax1.label,ax1.unit))
    plt.show()

def plot_3d(fname):
    call("plot3d %s %s"%(fname,' '.join(sys.argv[2:])))

if len(sys.argv) < 2:
    print("RSF plot\n%s [input rsf file: 1D/2D/3D]"%sys.argv[0])
    print("Options: transp=0, xflip=0, yflip=1, cmap='jet_r', spec=False (1d only)")
    sys.exit(1)

fname=sys.argv[1]
kwargs={}
for arg in sys.argv[2:]:
    k,v=arg.split('=')
    if v in ['False','0','']:
        v=False
    kwargs[k]=v

with rsf.input(fname) as f:
    ndim=f.dimension
    if ndim == 1:
        plot_1d(f,**kwargs)
    elif ndim == 2:
        plot_2d(f,**kwargs)
    elif ndim == 3:
        plot_3d(fname)

