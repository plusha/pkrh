import sys
import glob
import argparse
from subprocess import call
import shutil

args = sys.argv

if len(args) < 1:
    print("""
    tensorboard monitor: monitor ./runs/ directory
    list: list directories
    cat: cat latest log.txt
""")

def get_dirs(logdir='runs'):
    return glob.glob(logdir+'/*')

def get_log_dir(log=''):
    if log=='':
        dirs = get_dirs()
        dirs.sort()
        return dirs[-1]
    else:
        return log

def get_last_vel(dirname):
    vels=glob.glob(dirname+'/vel*')
    if len(vels) > 1:
        vels.sort()
        return vels[-1][-8:]
    else:
        return None

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-l','--list',action='store_const',const=True, default=False, help='list tensorboard log directory')
parser.add_argument('-t','--tail',action='store_const',const=True, default=False, help='tail(-f) latest log.txt')
#parser.add_argument('-c','--cat',action='store_const',const=True, default=False, help='cat latest log.txt')
parser.add_argument('-s','--start',action='store_const',const=True, default=False, help='start tensorboard')
parser.add_argument('-d','--delete',action='store_const',const=True, default=False, help='delete directories without velocities')
parser.add_argument('--logdir', type=str, default='runs',help='tensorboard log directory')
parser.add_argument('--log',type=str,default='',help='inspect specific log instead of the latest log')
args = parser.parse_args()

if args.list:
    dirs = get_dirs(args.logdir)
    dirs.sort()
    for d in dirs:
        vlast = get_last_vel(d)
        print("  %s [last velocity: %s]"%(d,vlast))
elif args.delete:
    for d in get_dirs(args.logdir):
        vlast = get_last_vel(d)
        if vlast is None:
            print("rm -r %s"%d)
            shutil.rmtree(d)
elif args.tail:
    d = get_log_dir(args.log)
    call('tail -f %s/log.txt'%d,shell=True)
elif args.start:
    cmd = 'tensorboard --logdir=%s'%args.logdir
    print(cmd)
    call(cmd,shell=True)
else:
    d = get_log_dir(args.log)
    call('cat %s/log.txt'%d,shell=True)
    print("\n//from %s/log.txt\n"%d)
