import sys
from subprocess import call
import matplotlib.pyplot as plt
import numpy as np
from pkrh.io import rsf

if len(sys.argv) < 2:
    print("RSF plot\n%s [input complex-type rsf file: 1D]"%sys.argv[0])
    sys.exit(1)

fname=sys.argv[1]

with rsf.input(fname) as f:
    data=f.read()
    ax=f.get("ax1")
    x=ax.o+np.arange(ax.n)*ax.d

    plt.plot(x,np.abs(data))
    plt.xlabel("%s [%s]"%(ax.label,ax.unit))
    plt.show()
