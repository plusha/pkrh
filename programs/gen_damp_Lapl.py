import numpy as np
from pkrh.io import rsf
from pkrh.io.optparse import Par

with Par("2D Laplace-Fourier domain damping constant file generator") as par:
    out=par.s('out',default='',msg='Output rsf file name')

    ns=par.i('ns',msg='Number of damping constants')
    s0=par.f('s0',msg='First damping constant (in Hz)')
    ds=par.f('ds',msg='Increments of dampings (in Hz)')

    fmt=par.s('fmt',default='%10.5f',msg='Output ascii format')

alpha=np.zeros(ns,dtype=np.float32)
for idamp in range(ns):
    alpha[idamp]=s0+idamp*ds

ax1=rsf.Axis(n=ns,d=1,label="Damping constant")

fout=rsf.output(out,ax1=ax1,form='ascii')
fout.write(alpha,close=True,fmt=fmt)
