import setuptools
import os

setuptools.setup(
        name='pkrh',
        version='0.1',
        description='PKNU research helper',
        author='Wansoo Ha',
        author_email='wansooha@gmail.com',
        packages=setuptools.find_packages(),
)
