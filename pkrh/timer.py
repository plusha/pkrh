import sys
import time
from functools import wraps

class Logger:
    def __init__(self,fout="log.out"):
        self.fout=open(fout,"w")

    def write(self,msg,stdout=True):
        log_msg="%s// %s"%(time.ctime(),msg)
        self.fout.write(log_msg+"\n")
        self.fout.flush()
        if stdout:
            print(log_msg)
            sys.stdout.flush()

    def final(self):
        self.fout.close()


class Timer:
    def __init__(self):
        self._accumulated = 0.
        self._start = 0.

    def reset(self):
        self.__init__()

    def start(self):
        self._start = time.time()

    def elapsed(self):
        return time.time() - self._start

    def report(self, msg, fmt="%f", accumulated=False):
        if accumulated:
            _time = self._accumulated
        else:
            _time = self.elapsed()
            self._accumulated += _time
        print(msg, fmt % _time)

    def print(self, msg):
        print(time.ctime(),": ",msg)


def tprint(*msg,flush=True):
    print(time.ctime(),": ",*msg)
    if flush:
        sys.stdout.flush()


def timefn(fn):
    @wraps(fn)
    def measure_time(*args, **kwargs):
        t1 = time.time()
        result = fn(*args, **kwargs)
        t2 = time.time()
        print("@timefn: '{}' took {} sec.".format(fn.__name__, t2 - t1))
        return result

    return measure_time
