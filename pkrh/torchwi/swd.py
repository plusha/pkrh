import numpy as np
import torch

def get_iswd(swdep_meter,h_km):
    to_km = 0.001
    iswd = (swdep_meter*to_km/h_km).astype(np.int32)
    return iswd

def get_land_mask(swdep_meter,nx,ny,h_km):
    iswd = get_iswd(swdep_meter,h_km)
    land_mask = torch.ones(nx,ny,dtype=torch.int32)
    for ix in range(nx):
        land_mask[ix,0:iswd[ix]]=0
    return land_mask
