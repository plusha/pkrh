import torch
import numpy as np

def optim_selector(name,parameters,lr,momentum=0.9):
    if name == 'adam':
        optim= torch.optim.Adam(parameters, lr=lr)
    elif name == 'sgd':
        optim= torch.optim.SGD(parameters, lr=lr)
    elif name == 'momentum':
        optim= torch.optim.SGD(parameters, lr=lr, momentum=momentum)
    elif name == 'nag':
        optim= torch.optim.SGD(parameters, lr=lr, momentum=momentum, nesterov=True)
    else:
        raise ValueError("Wrong optimizer name")
    return optim

# bound constraint for scipy.optim.minimize
def bound(vmin,vmax,n):
    return [(vmin,vmax)]*n

def bound_swd(vmin,vmax,vwater,iswd,nx,ny):
    bnd = np.zeros((nx,ny,2))
    bnd[:,:,0]=vmin
    bnd[:,:,1]=vmax
    for ix in range(nx):
        bnd[ix,:iswd[ix],:]=vwater
    bnd.shape=(nx*ny,2)
    return bnd
