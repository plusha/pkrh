import numpy as np
import torch
from torchwi.parameter import VelocityParameter
from torchwi.parameter import SlownessParameter
from torchwi.parameter import SlothParameter

def param_selector(name,vinit,vmin=None,vmax=None):
    if type(vinit) == np.ndarray:
        vinit = torch.from_numpy(vinit)

    if name == 'velocity':
        param = VelocityParameter(vinit,vmin,vmax)
    elif name == 'slowness':
        param = SlownessParameter(vinit,vmin,vmax)
    elif name == 'sloth':
        param = SlothParameter(vinit,vmin,vmax)
    else:
        raise ValueError("Wrong parameter name")
    return param
