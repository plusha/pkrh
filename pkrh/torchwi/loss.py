from torchwi.loss import LaplLogLoss
from pkrh.loss import LaplUDLoss, LaplDULoss
from pkrh.loss import TotalVariation

def loss_selector(name,power=1.0):
    if name == 'logarithmic' or name == 'log':
        loss_func = LaplLogLoss.apply
    elif name == 'totalvariation' or name == 'tv':
        loss_func = TotalVariation()
    elif name == 'ud' or name == 'du':
        if   name == 'ud': loss_f = LaplUDLoss.apply
        elif name == 'du': loss_f = LaplDULoss.apply
        def loss_func(frd, true):
            return loss_f(frd, true, power)
    else:
        raise ValueError("Wrong loss function name")
    return loss_func
