import os, sys
import numpy as np
from pkrh.io import errexit

SU_KEYWORDS = [
    ('tracl', np.int32),
    ('tracr', np.int32),
    ('fldr', np.int32),
    ('tracf', np.int32),
    ('ep', np.int32),
    ('cdp', np.int32),
    ('cdpt', np.int32),
    ('trid', np.int16),
    ('nvs', np.int16),
    ('nhs', np.int16),
    ('duse', np.int16),
    ('offset', np.int32),
    ('gelev', np.int32),
    ('selev', np.int32),
    ('sdepth', np.int32),
    ('gdel', np.int32),
    ('sdel', np.int32),
    ('swdep', np.int32),
    ('gwdep', np.int32),
    ('scalel', np.int16),
    ('scalco', np.int16),
    ('sx', np.int32),
    ('sy', np.int32),
    ('gx', np.int32),
    ('gy', np.int32),
    ('counit', np.int16),
    ('wevel', np.int16),
    ('swevel', np.int16),
    ('sut', np.int16),
    ('gut', np.int16),
    ('sstat', np.int16),
    ('gstat', np.int16),
    ('tstat', np.int16),
    ('laga', np.int16),
    ('lagb', np.int16),
    ('delrt', np.int16),
    ('muts', np.int16),
    ('mute', np.int16),
    ('ns', np.uint16),
    ('dt', np.uint16),
    ('gain', np.int16),
    ('igc', np.int16),
    ('igi', np.int16),
    ('corr', np.int16),
    ('sfs', np.int16),
    ('sfe', np.int16),
    ('slen', np.int16),
    ('styp', np.int16),
    ('stas', np.int16),
    ('stae', np.int16),
    ('tatyp', np.int16),
    ('afilf', np.int16),
    ('afils', np.int16),
    ('nofilf', np.int16),
    ('nofils', np.int16),
    ('lcf', np.int16),
    ('hcf', np.int16),
    ('lcs', np.int16),
    ('hcs', np.int16),
    ('year', np.int16),
    ('day', np.int16),
    ('hour', np.int16),
    ('minute', np.int16),
    ('sec', np.int16),
    ('timbas', np.int16),
    ('trwf', np.int16),
    ('grnors', np.int16),
    ('grnofr', np.int16),
    ('grnlof', np.int16),
    ('gaps', np.int16),
    ('otrav', np.int16),
    ## su variation
    ('d1', np.float32),
    ('f1', np.float32),
    ('d2', np.float32),
    ('f2', np.float32),
    ('ungpow', np.float32),
    ('unscale', np.float32),
    ('ntr', np.int32),
    ('mark', np.int16),
    ('shortpad', np.int16),
    ('unass', (np.int16, 14)),
    ## SEGY rev1
    # ('cdpx', np.int32),
    # ('cdpy', np.int32),
    # ('Inline3D', np.int32),
    # ('Crossline3D', np.int32),
    # ('ShotPoint', np.int32),
    # ('ShotPointScalar', np.int16),
    # ('TraceValueMeasurementUnit', np.int16),
    # ('TransductionConstantMantissa', np.int32),
    # ('TransductionConstantPower', np.int16),
    # ('TransductionUnit', np.int16),
    # ('TraceIdentifier', np.int16),
    # ('ScalarTraceHeader', np.int16),
    # ('SourceType', np.int16),
    # ('SourceEnergyDirectionMantissa', np.int32),
    # ('SourceEnergyDirectionExponent', np.int16),
    # ('SourceMeasurementMantissa', np.int32),
    # ('SourceMeasurementExponent', np.int16),
    # ('SourceMeasurementUnit', np.int16),
    # ('UnassignedInt1', np.int32),
    # ('UnassignedInt2', np.int32),
]
SU_HEADER_DTYPE = np.dtype(SU_KEYWORDS)
SU_KEY_LIST = [keytype[0] for keytype in SU_KEYWORDS]
SU_HEADER_BYTES = 240


def input(filename='', mode='rb'):
    # get ns
    if filename:
        with open(filename, 'rb') as f:
            header = np.fromfile(f, dtype=SU_HEADER_DTYPE, count=1)
    else:
        header = np.fromfile(sys.stdin, dtype=SU_HEADER_DTYPE, count=1)
        # sys.stdin.seek(offset=0,whence=0)
        sys.stdin.seek(0)
    ns = header['ns'][0]
    return SuFile(ns=ns, filename=filename, mode=mode)


def output(ns, filename='', mode='wb'):
    return SuFile(ns=ns, filename=filename, mode=mode)


def fromfile(filename='', key=''):
    with input(filename) as f:
        traces = f.read(count=-1)
    if key:
        key = key.split()
        if len(key) == 1:
            return traces[key[0]]
        else:
            return [traces[k] for k in key]
    else:
        return traces


def tofile(filename, traces):
    ns = traces[0]['ns']
    with output(ns, filename) as f:
        f.write(traces)


def su_dtype(ns):
    return np.dtype(SU_HEADER_DTYPE.descr + [('data', ('f4', ns))])


def trace(**kwargs):
    try:
        ns = kwargs['ns']
    except:
        errexit("You need to set 'ns' to generate a trace")
    trc = np.zeros(1, dtype=su_dtype(ns))
    for k, v in kwargs.items():
        trc[k] = v
    return trc.view(np.recarray)


def get(trc, key):
    return trc[key][0]


def set(trc, **kwargs):
    for k, v in kwargs.items():
        trc[k] = v
    return trc

def scale_write(x,scalco):
    if scalco > 0:
        return int(np.around(x/scalco))
    elif scalco < 0:
        return int(np.around(-x*scalco))
    else: #scalco=0
        return int(np.around(x))

def scale_read(x,scalco):
    if scalco > 0:
        return np.float32(x*scalco)
    elif scalco < 0:
        return np.float32(-x/scalco)
    else: #scalco=0
        return np.float32(x)


class SuFile:
    def __init__(self, ns, filename='', mode='rb'):
        self.std = False
        if filename:
            self.f = open(filename, mode)
        else:
            self.std = True
            if mode == 'rb':
                self.f = sys.stdin
            else:  # 'wb','r+b'
                self.f = sys.stdout
        self.filename = filename
        # define structure of a structured array
        self.ns = ns
        # self.dtype = np.dtype(SU_HEADER_DTYPE.descr + [('data',('f4',ns))])
        self.dtype = su_dtype(ns)
        self.trace_bytes = SU_HEADER_BYTES + 4 * ns

    # for with statement
    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.close()

    @property
    def size(self):
        if not self.std:
            return os.path.getsize(self.filename)
        else:
            print("No size for std")
            return 0

    @property
    def ntrace(self):
        return int(self.size / self.trace_bytes)

    def read(self, itr=None, count=1):  # count=-1 to read all traces
        if itr is not None:
            self._move_file_pointer(itr)
        # np.fromfile returns empty array after eof
        return np.fromfile(self.f, dtype=self.dtype, count=count).view(np.recarray)

    @property
    def data(self):
        self._move_file_pointer(0)
        return np.fromfile(self.f, dtype=self.dtype)['data']

    def traces(self):
        trc = self.read()
        while len(trc) > 0:
            yield trc
            trc = self.read()

    def _move_file_pointer(self, itr):
        # self.f.seek(offset=itr*self.trace_bytes,whence=0)
        self.f.seek(itr * self.trace_bytes)

    def read_header(self, itr):
        self._move_file_pointer(itr)
        return np.fromfile(self.f, dtype=SU_HEADER_DTYPE, count=1).view(np.recarray)

    def write(self, trace, itr=None):
        if itr is not None:
            self._move_file_pointer(itr)
        trace.tofile(self.f)

    def close(self):
        if not self.std:
            self.f.close()
