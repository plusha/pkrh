import re, sys


def quote(str):
    return '"' + str + '"'


def unquote(str):
    if str.startswith("'") or str.startswith('"'):
        return str[1:-1]
    else:
        return str


def fromstring(key, text, un_quote=True):
    try:
        val = re.findall(key + "\s*=\s*(\S+)", text)[-1]
        # val=re.findall(key+"=(\S+)",text)[-1]
        for q in ["'", '"']:
            if val.startswith(q) and not val.endswith(q):
                val = re.findall(key + "=" + q + "(.+?)" + q, text)[-1]
        if un_quote:
            val = unquote(val)
        return val, True
    except:
        return 0, False


def fromlist(key, lst, un_quote=True):
    val, found = 0, False
    for arg in lst:
        if arg.startswith(key + '='):
            val = arg.split('=', 1)[1]
            found = True
    if found:
        if un_quote:
            val = unquote(val)
    return val, found


def file2str(filename=''):
    if filename and filename.lower() != 'stdin':
        with open(filename, 'r') as fh:
            string = fh.read()
    else:
        string = sys.stdin.read()
    return string


def str2file(string, filename=''):
    if filename and filename.lower() != 'stdout':
        with open(filename, 'w') as fh:
            fh.write(string)
    else:
        sys.stdout.write(string)


def str2bool(val):
    if val.strip().lower() in ['true', 't', 'yes', 'on', '1']:
        return True
    else:
        return False
