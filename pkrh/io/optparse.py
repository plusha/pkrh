import sys
from pkrh.io import errexit
from pkrh.io.string_utils import unquote, fromstring, fromlist, str2bool


class Par:
    def __init__(self, header, filename=None):
        if filename:
            self.parser = fromstring
            with open(filename, 'r') as f:
                self.arg = f.read()
        else:
            self.parser = fromlist
            self.arg = sys.argv[1:]
        self.helpmsg = '\n  %s\n  Required parameters:' % (header)
        self.helpmsg_optional = '  Optional parameters:'
        self.read_ok = True

    def add_msg(self, key, typ, default, msg):
        if default is None:
            self.helpmsg += "\n    [%s] %s\t: %s" % (typ, key, msg)
        else:
            self.helpmsg_optional += "\n    [%s] %s = %s\t: %s" % (typ, key, default, msg)

    def help(self, footer=''):
        if not self.read_ok or '-h' in sys.argv:
            errexit("%s\n%s\n%s" % (self.helpmsg, self.helpmsg_optional, footer))
            raise KeyError

    def _conv2type(self, conv):
        if conv == int:
            typ = 'i'
        elif conv == float:
            typ = 'f'
        elif conv == str:
            typ = 's'
        elif conv == str2bool:
            typ = 'b'
        else:
            typ = ''
        return typ

    def get(self, key, conv=str, default=None, msg='', typ=None, un_quote=True):
        if not typ:
            typ = self._conv2type(conv)
        self.add_msg(key, typ, default, msg)
        val, found = self.parser(key, self.arg, un_quote)
        if found:
            return conv(val), True
        elif default is None:
            self.read_ok = False
            return 0, False
        else:
            return default, False

    def getlist(self, key, conv=str, default=None, msg=''):
        val, found = self.get(key, str, default, msg, typ=self._conv2type(conv), un_quote=False)
        if found:
            lst = val.split(',')
            return [conv(item) for item in lst], True
        else:
            return default, False

    def i(self, key, default=None, msg=''):
        return self.get(key, int, default, msg)[0]

    def f(self, key, default=None, msg=''):
        return self.get(key, float, default, msg)[0]

    def s(self, key, default=None, msg=''):
        return self.get(key, str, default, msg)[0]

    def b(self, key, default=None, msg=''):
        return self.get(key, str2bool, default, msg)[0]

    def ilist(self, key, default=None, msg=''):
        return self.getlist(key, int, default, msg)[0]

    def flist(self, key, default=None, msg=''):
        return self.getlist(key, float, default, msg)[0]

    def blist(self, key, default=None, msg=''):
        return self.getlist(key, str2bool, default, msg)[0]

    def slist(self, key, default=None, msg=''):
        return [unquote(st) for st in self.getlist(key, str, default, msg)[0]]

    # for with statement
    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.help()
