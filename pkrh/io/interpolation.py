import sys
import numpy as np
from numba import jit
from scipy.interpolate import interp1d
from pkrh.io import su, progress_bar

@jit
def accumulate(seismo):
    acc_sm=np.zeros_like(seismo)
    nt,nx=seismo.shape
    acc=np.zeros(nx,dtype=np.float32)
    for it in range(nt):
        acc += np.abs(seismo[it])**2
        acc_sm[it,:]=acc[:]
    return acc_sm

class SU_handler:
    def __init__(self,filename,logger=None):
        self.logger=logger
        if self.logger:
            self.logger.print("reading header of %s"%filename)
        self.sf = su.input(filename)
        self.ntrace = self.sf.ntrace
        if self.logger:
            self.logger.print("number of traces: %d"%self.ntrace)

        fldr = np.zeros(self.ntrace,dtype=np.int32)
        sx = np.zeros(self.ntrace)
        sy = np.zeros(self.ntrace)
        self.gx_list = np.zeros(self.ntrace)
        swdep = np.zeros(self.ntrace)

        if self.logger:
            self.logger.print("Reading header infomation")

        for itr in range(self.ntrace):
            progress_bar(itr,self.ntrace)
            header = self.sf.read_header(itr)
            if itr == 0:
                self.nt = su.get(header,'ns')
                self.dt = su.get(header,'dt')/1.e6
            scalco = su.get(header,'scalco')
            scalel = su.get(header,'scalel')
            fldr[itr] = su.get(header,'fldr')
            sx[itr] = su.scale_read(su.get(header,'sx'),scalco)
            sy[itr] = su.scale_read(su.get(header,'sy'),scalco)
            self.gx_list[itr] = su.scale_read(su.get(header,'gx'),scalco)
            swdep[itr] = su.scale_read(su.get(header,'swdep'),scalel)

        # fldr_unique: unique fldr values used to seperate shot gathers
        # shot_idx: first occurences of unique fldr values
        # ntrcs: number of traces in each shot gather
        fldr_unique,self.shot_idx,self.ntrcs = np.unique(fldr,return_index=True,return_counts=True)

        self.nshot=len(fldr_unique)
        if self.logger:
            self.logger.print("SU - number of shots: %d, nt=%d, dt=%s"%(self.nshot,self.nt,self.dt))

        self.sx_list = sx[self.shot_idx]
        self.sy_list = sy[self.shot_idx]
        self.swd = swdep[self.shot_idx]

    def get_receiver_coordinate(self,ishot):
        istart = self.shot_idx[ishot]
        iend = istart + self.ntrcs[ishot]
        ax = self.gx_list[istart:iend]
        return ax

    def get_shot(self,ishot):
        nrcv = self.ntrcs[ishot]
        ishot_start = self.shot_idx[ishot]

        trc = self.sf.read(itr=ishot_start,count=nrcv)
        return trc['data']

    def close(self):
        self.sf.close()

def get_shot_weight(sx,h,nx,xmin):
    loc = np.array([sx-h,sx,sx+h])
    wgt = np.array([0.,1.,0.])
    fintp = interp1d(loc,wgt)
    for ix in range(nx):
        x = xmin+ix*h
        if x<=sx and sx<x+h:
            pos=np.array([x,x+h])
            return np.concatenate((pos-xmin,fintp(pos)))
    print("Cannot find shot location!! (sx=%s)"%sx)
    sys.exit(1)

