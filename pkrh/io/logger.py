import sys
import logging
import logging.handlers


def progress_bar(count,total,status=''):
    # from https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
    bar_len=60
    frac = count/total
    filled_len = int(round(bar_len*frac))
    percents = round(100*frac,1)
    bar = '='*filled_len + '-'*(bar_len-filled_len)

    sys.stdout.write('[%s] %s%% ...%s\r'%(bar,percents,status))
    sys.stdout.flush()


class Logger:
    def __init__(self, filename="log.txt", name='pkgpl', print_name=False, level=logging.DEBUG):
        fileHandler = logging.FileHandler(filename)
        streamHandler = logging.StreamHandler()

        if print_name:
            formatter = logging.Formatter('[%(asctime)s] %(name)s: %(message)s')
        else:
            formatter = logging.Formatter('[%(asctime)s] %(message)s')
        fileHandler.setFormatter(formatter)
        streamHandler.setFormatter(formatter)

        self.logger = logging.getLogger(name)
        self.logger.addHandler(fileHandler)
        self.logger.addHandler(streamHandler)

        self.logger.setLevel(level)
        self.info("Starting %s"%name)

    def print(self, msg, *args, **kwargs):
        self.debug(msg, *args, **kwargs)

    def debug(self, msg, *args, **kwargs):
        self.logger.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.logger.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.logger.error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.logger.critical(msg, *args, **kwargs)


