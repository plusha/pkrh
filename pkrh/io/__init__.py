import sys
from .logger import Logger, progress_bar
from .cfgparse import CFGParser

def errexit(msg, errno=1):
    warn(msg)
    sys.exit(errno)


def warn(msg):
    sys.stderr.write(msg + "\n")


