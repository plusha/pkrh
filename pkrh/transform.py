import numpy as np
#from numba import jit


#@jit
def laplace_transform(w, dt, alpha):
    nt = len(w)
    at = np.arange(nt) * dt
    cw = np.zeros(len(alpha), dtype=np.float64)
    for ifreq, ss in enumerate(alpha):
        expf = np.exp(-ss * at)
        cw[ifreq] = np.dot(w, expf) * dt
    return cw


#@jit
def laplace_fourier_transform(w, dt, alpha):
    nt = len(w)
    at = np.arange(nt) * dt
    cw = np.zeros(len(alpha), dtype=np.complex128)
    for ifreq, omega in enumerate(alpha):
        expf = np.exp(-1.0j * omega * at)
        cw[ifreq] = np.dot(w, expf) * dt
    return cw

#@jit
def gen_lt_table(at,damps,dt):
    table=np.zeros((len(damps),len(at)),dtype=np.float64)
    for ifreq,s in enumerate(damps):
        table[ifreq,:]=np.exp(-s*at)*dt
    return table

#@jit
def gen_lft_table(at,omegas,dt):
    table = np.zeros((damps.shape[0],len(at)),dtype=np.complex128)
    for ifreq, w in enumerate(omegas):
        omega = w[0] + w[1]*1.0j
        table[ifreq,:] = np.exp(-1.0j * omega * at)
    return table

#@jit
def integral_transform(seismo,table_ifreq):
    nx=seismo.shape[0]
    data = np.zeros(nx,dtype=table_ifreq.dtype)
    for ix in range(nx):
        data[ix] = np.dot(seismo[ix,:],table_ifreq)
    return data

