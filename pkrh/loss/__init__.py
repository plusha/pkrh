from .total_variation import TotalVariation
from .DeconResid import LaplUDLoss, LaplDULoss
