import torch

torch_zero = torch.tensor(0.,dtype=torch.float64)
tolmin=1.e-100

# r = u/d - 1
# backprop = r / d
class ResidUD(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true, tolmin=1.e-100):
        #nrhs, nx = true.shape
        resid = torch.where(torch.abs(true)>tolmin, frd/true-1, torch_zero)

        ctx.save_for_backward(true,torch.tensor(tolmin))
        return resid

    @staticmethod
    def backward(ctx, grad_output):
        true,tolmin = ctx.saved_tensors
        grad_input = torch.where(torch.abs(true)>tolmin, grad_output/true, torch_zero)
        return grad_input, None, None


# r = d/u - 1
# backprop = -d/u**2 * r
class ResidDU(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true, tolmin=1.e-100):
        #nrhs, nx = true.shape
        resid = torch.where(torch.abs(frd)>tolmin, true/frd-1, torch_zero)

        ctx.save_for_backward(frd,true,torch.tensor(tolmin))
        return resid

    @staticmethod
    def backward(ctx, grad_output):
        frd,true,tolmin = ctx.saved_tensors
        grad_input = torch.where(torch.abs(frd)>tolmin, -true/frd**2 * grad_output, torch_zero)
        return grad_input, None, None


# r = u/d - 1
# backprop = r / d
class ResidUDpower(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true, p=0.1, tolmin=1.e-100):
        #nrhs, nx = true.shape
        resid = torch.where(torch.abs(true)>tolmin, torch.abs(frd/true)**p - 1, torch_zero)
        #resid = torch.where(torch.abs(true)>tolmin, (frd/true)**p - 1, torch_zero)

        ctx.save_for_backward(frd,true,torch.tensor(p),torch.tensor(tolmin))
        return resid

    @staticmethod
    def backward(ctx, grad_output):
        frd,true,p,tolmin = ctx.saved_tensors
        grad_input = torch.where(torch.abs(true)>tolmin, grad_output* p/torch.abs(true)*torch.abs(frd/true)**(p-1), torch_zero)
        #grad_input = torch.where(torch.abs(true)>tolmin, grad_output* p/true*(frd/true)**(p-1), torch_zero)
        return grad_input, None, None, None


# r = d/u - 1
# backprop = -d/u**2 * r
class ResidDUpower(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true, p=0.1, tolmin=1.e-100):
        #nrhs, nx = true.shape
        resid = torch.where(torch.abs(frd)>tolmin, torch.abs(true/frd)**p - 1, torch_zero)
        #resid = torch.where(torch.abs(frd)>tolmin, (true/frd)**p - 1, torch_zero)

        ctx.save_for_backward(frd,true,torch.tensor(p),torch.tensor(tolmin))
        return resid

    @staticmethod
    def backward(ctx, grad_output):
        frd,true,p,tolmin = ctx.saved_tensors
        grad_input = torch.where(torch.abs(frd)>tolmin, grad_output* (-p/frd)*torch.abs(true/frd)**p, torch_zero)
        #grad_input = torch.where(torch.abs(frd)>tolmin, grad_output* (-p/frd)*(true/frd)**p, torch_zero)
        return grad_input, None, None, None


# r = u/d - 1
# backprop = r / d
class LaplUDLoss(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true, p=0.1):
        #nrhs, nx = true.shape
        mask = (torch.abs(frd)>tolmin) & (torch.abs(true)>tolmin)
        resid = torch.where(mask, torch.abs(frd/true)**p - 1, torch_zero)
        loss = 0.5*torch.sum(resid**2)
        ctx.save_for_backward(frd,true,resid,torch.tensor(p),mask)
        return loss

    @staticmethod
    def backward(ctx, grad_output):
        frd,true,resid,p,mask = ctx.saved_tensors
        grad_input = torch.where(mask, resid * p/torch.abs(true)*torch.abs(frd/true)**(p-1), torch_zero)
        return grad_input, None, None


# r = d/u - 1
# backprop = -d/u**2 * r
class LaplDULoss(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true, p=0.1):
        #nrhs, nx = true.shape
        mask = (torch.abs(frd)>tolmin) & (torch.abs(true)>tolmin)
        resid = torch.where(mask, torch.abs(true/frd)**p - 1, torch_zero)
        loss = 0.5*torch.sum(resid**2)
        ctx.save_for_backward(frd,true,resid,torch.tensor(p),mask)
        return loss

    @staticmethod
    def backward(ctx, grad_output):
        frd,true,resid,p,mask = ctx.saved_tensors
        grad_input = torch.where(mask, resid * (-p/frd)*torch.abs(true/frd)**p, torch_zero)
        return grad_input, None, None


