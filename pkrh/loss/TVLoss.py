# modified tv from https://github.com/kornia/kornia
import torch
import torch.nn as nn


class TotalVariationLoss(nn.Module):
    r"""Computes the Total Variation according to [1].

    Shape:
        - Input: :math:`(H, W)`
        - Output: :math:`()`

    Reference:
        [1] https://en.wikipedia.org/wiki/Total_variation
    """

    def __init__(self) -> None:
        super().__init__()

    def forward(  # type: ignore
            self, img) -> torch.Tensor:
        return total_variation2d(img)


def total_variation2d(img: torch.Tensor) -> torch.Tensor:
    r"""Function that computes Total Variation.

    See :class:`~kornia.losses.TotalVariation` for details.
    """
    if not torch.is_tensor(img):
        raise TypeError(f"Input type is not a torch.Tensor. Got {type(img)}")
    img_shape = img.shape
    if len(img_shape) == 2:
        pixel_dif1 = img[1:, :] - img[:-1, :]
        pixel_dif2 = img[:, 1:] - img[:, :-1]
        reduce_axes = (-2, -1)
    else:
        raise ValueError("Expected input tensor to be of ndim 2, but got " + str(len(img_shape)))

    return pixel_dif1.abs().sum(dim=reduce_axes) + pixel_dif2.abs().sum(dim=reduce_axes)
