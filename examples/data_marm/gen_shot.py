import numpy as np
from torchwi.io import rsf

out='shot.rsf'
nshot=95 # number of shots
sx0=0.096 # first source location (in km)
dsx=0.096 # source interval
sz=0.016 # source depth

# 2d
shot=np.zeros((nshot,2),dtype='f')
for ishot in range(nshot):
    shot[ishot,0]=sx0+ishot*dsx
    shot[ishot,1]=sz

ax1=rsf.Axis(n=2,d=1,label="Dimension")
ax2=rsf.Axis(n=nshot,d=1,label="Shot number")

fout=rsf.output(out,ax1=ax1,ax2=ax2,form='ascii')
fout.write(shot,close=True,fmt="%10.5f")
