from torchwi.io import rsf
from torchwi.utils import wavelets

out='wavelet.rsf'
nt=3000
dt=0.001
fmax=15.
wav='ricker'

if wav == "ricker":
    w=wavelets.ricker(fmax,dt,nt,dtype='f')
elif wav == "fdgaus":
    w=wavelets.fdgaus(fmax,dt,nt,dtype='f')

at=rsf.Axis(n=nt,d=dt,label='Time',unit='s')

with rsf.output(out,ax1=at,form='ascii') as fout:
    fout.write(w)
