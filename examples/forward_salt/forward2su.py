import sys
import numpy as np
from pkrh.io import rsf, su, CFGParser, Logger

try:
    fcfg=sys.argv[1]
except:
    fcfg='par_at2d.ini'

logger=Logger("log_frd2su.txt","frd2su")
logger.print("reading %s"%fcfg)

cfg=CFGParser(fcfg)
pdict = cfg.parse('Time-domain Modeling')
sdict = cfg.parse('SU')

fseismo = pdict['fseismo']
nx,h = rsf.fromfile(pdict['fvel'],'n2 d2')
nt,dt = rsf.fromfile(pdict['fwav'],'n1 d1')

h_meter = h*1000
dt_microsec = int(dt*1.e6)

# read shot position
nshot,src = rsf.fromfile(pdict['fshot'],'n2 data')
scalco=sdict['scalco']
swdep = sdict.get('swdep',0)
iswdep = int(np.around(swdep*1000))

logger.print("nshot=%d, nx=%d, h=%s, nt=%d, dt=%s"%(nshot,nx,h_meter,nt,dt_microsec))
logger.print("forward file = %s"%fseismo)
logger.print("scalco = %s"%scalco)
if swdep > 0:
    logger.print("swdep = %s"%iswdep)
logger.print("output file = %s"%sdict['fsu'])

# output su file
so = su.output(ns=nt, filename=sdict['fsu'])

for ishot in range(nshot):
    sx,sy = src[ishot]
    sx_meter = sx*1000
    isx = su.scale_write(sx_meter,scalco)
    logger.print("ishot=%d, sx=%s"%(ishot,su.scale_read(isx,scalco)))
    seismo = np.fromfile("%s%04d"%(fseismo,ishot),dtype=np.float32)
    seismo.shape = (nt,nx)
    for ix in range(nx):
        igx = su.scale_write(ix * h_meter,scalco)
        trc = su.trace(ns=nt,dt=dt_microsec, tracf=ix+1,fldr=ishot+1, 
                sx=isx,gx=igx,scalco=scalco, swdep=iswdep,
                data=seismo[:,ix])
        so.write(trc)

so.close()
