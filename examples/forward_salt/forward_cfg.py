import torch
import horovod.torch as hvd
import sys
from pathlib import Path
from torchwi import Time2d
from torchwi.io import rsf, CFGParser
from torchwi.io import time_forward_distributed_dataloader

try:
    fcfg=sys.argv[1]
except:
    fcfg='par_at2d.ini'

hvd.init()

pdict = CFGParser(fcfg).parse('Time-domain Modeling')

nt,dt,w = rsf.fromfile(pdict['fwav'],"n1 d1 data")
ny,nx,h,vel = rsf.fromfile(pdict['fvel'],"n1 n2 d1 data")
nshot,sxy = rsf.fromfile(pdict['fshot'],"n2 data")

if hvd.rank()==0:
    Path(pdict['outdir']).mkdir(exist_ok=True)
    print("npe=%s"%hvd.size())
    print("nt=%s, dt=%s"%(nt,dt))
    print("ny=%s, nx=%s, h=%s"%(ny,nx,h))
    print("nshot=%s"%nshot)
    print("order=%d"%pdict['order'])

DEVICE=pdict['device']

if DEVICE=='cuda' and torch.cuda.is_available():
    torch.cuda.set_device(hvd.local_rank())

vp=torch.from_numpy(vel).to(DEVICE)
w=torch.from_numpy(w)
sxy=torch.from_numpy(sxy).to(DEVICE) # source x, y coordinate

dataloader=time_forward_distributed_dataloader(sxy)

model = Time2d(nx,ny,h,w,dt,pdict['order'],DEVICE)
hvd.broadcast_parameters(model.state_dict(), root_rank=0)

with torch.no_grad():
    for sx,sy, ishot in dataloader:
        print("rank=%d, ishot=%d, sx=%s"%(hvd.rank(),ishot,sx))
        ry = sy
        seismo = model(vp, sx,sy,ry)
        seismo.to('cpu').detach().numpy().tofile("%s%04d"%(pdict['fseismo'],ishot))
