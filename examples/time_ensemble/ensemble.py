import torch

class EnsembleParameter2(torch.nn.Module):
    def __init__(self, generators, alpha=0.5):
        super().__init__()
        self.gen1=generators[0]
        self.gen2=generators[1]
        self.alpha = torch.nn.Parameter(torch.tensor(alpha))

    def vel(self):
        v1 = self.gen1() * self.alpha
        v2 = self.gen2() * (1.-self.alpha)
        return v1 + v2

    def forward(self, weights=None):
        return self.vel()

    def report(self):
        return "Ensemble: %s"%([gen.report() for gen in self.generators])

