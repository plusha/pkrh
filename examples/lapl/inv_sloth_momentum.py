import sys
import torch
import numpy as np
from pathlib import Path
from torchwi import Lapl2d
from torchwi.loss import LaplLogLoss
from torchwi.source import SourceEstimationLaplLog
from torchwi.parameter import SlothParameter
from torchwi.io import FreqDataset as Dataset
from torchwi.io import rsf, CFGParser
from torchwi.io import MainLogger

try:
    fcfg=sys.argv[1]
except:
    fcfg='par_inverse.ini'

pdict = CFGParser(fcfg).parse('Laplace-domain Inversion')
mdict = CFGParser(pdict['fintpl']).parse_sections('Interpolation','Output','Laplace')

# read info - src, initial model, damp
logger=MainLogger(name='lwi2d',comment='momentum')
logdir=logger.get_logdir()
logger.print("Inversion start")
logger.print("logdir = %s"%logdir)
logger.print("hyperparameters: %s"%pdict)
logger.print("data: %s"%mdict)

nrhs=pdict['nrhs']
num_epoch = pdict['maxiter']
vmin,vmax=pdict['vmin'],pdict['vmax']
lr = pdict['steplength']

ny,nx,h,vinit = rsf.fromfile(pdict['finit'],'n1 n2 d1 data')

nfreq, damps = rsf.fromfile(mdict['laplace_coeff_file'],'n1 data')
nshot, sxy = rsf.fromfile(mdict['shot_file'],'n2 data')
to_km = 0.001
sx_all = sxy[:,0]*to_km
sy = sxy[0,1]*to_km
ry = sy

logger.print("nfreq=%d"%nfreq)
logger.print("nshot=%d"%nshot)
logger.print("nx=%d, ny=%d"%(nx,ny))

vel = SlothParameter(torch.from_numpy(vinit),vmin,vmax)

optimizer = torch.optim.SGD(vel.parameters(), lr=lr, momentum=0.9)
src_estm = [SourceEstimationLaplLog() for s in damps]
dataset = [Dataset("%s%03d"%(mdict['laplace_file'],ifreq),sx_all,dtype=np.float64) for ifreq,_ in enumerate(damps)]
log_loss = LaplLogLoss.apply

# initial source estimation
logger.print("Source estimation")
total_loss = 0.
for ifreq,(s,source) in enumerate(zip(damps,src_estm)):
    model = Lapl2d(nx,ny,h)
    model.factorize(s,vel())
    dataloader = torch.utils.data.DataLoader(dataset[ifreq],batch_size=nrhs)
    loss = 0.
    for sxs,true in dataloader:
        frd = model(sxs,sy,ry,source.amplitude())
        loss1, resid = log_loss(frd, true)
        loss += loss1
        source.add(resid)
    total_loss += loss.item()
    source.step()
    model.finalize()
    logger.print("ifreq = %d, source amplitude = %s"%(ifreq,source.amplitude()))
logger.print("Total loss before source estimation = %s"%total_loss)


# main loop
for itr in range(num_epoch+1):

    # frequency loop
    total_loss = 0.
    optimizer.zero_grad()

    for ifreq,(s,source) in enumerate(zip(damps,src_estm)):
        model = Lapl2d(nx,ny,h)
        model.factorize(s,vel())
        dataloader = torch.utils.data.DataLoader(dataset[ifreq],batch_size=nrhs)

        loss = 0.
        # shot loop
        for sxs,true in dataloader:
            frd = model(sxs,sy,ry,source.amplitude())

            loss1, resid = log_loss(frd, true)

            loss += loss1
            source.add(resid)

        total_loss += loss.item()
        # source estimation
        source.step()
        # gradient calculation
        loss.backward()
        model.finalize()

    if itr==0:
        gnorm0 = vel.grad_norm()
        total_loss0=total_loss
    # all freq grad
    vel.normalize_gradient(gnorm0)
    optimizer.step()

    logger.progress_bar(itr,pdict['maxiter'],"norm_loss=%9.3e"%(total_loss/total_loss0))
    logger.log_loss(total_loss,itr,name='loss')

    if itr < 10 or itr % pdict['skip_output'] == 0:
        logger.log_velocity(vel(),itr,h,vmin=pdict['vmin'],vmax=pdict['vmax'])
        logger.log_gradient(vel.gradient(), itr, h)

        sources = np.array([src.amplitude() for src in src_estm])
        rsf.tofile("%s/source.%04d.rsf"%(logdir,itr),data=sources,form='ascii',ax1=rsf.Axis(n=nfreq,d=1,label='ifreq'))

logger.print("\nEnd")
