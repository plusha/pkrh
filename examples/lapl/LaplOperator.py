import torch
import numpy as np
from ld2d_base_fdm_pml import impedance_matrix_vpad
from pardiso_solver import PardisoSolver

class Lapl2d(torch.nn.Module):
    def __init__(self,nx,ny,h,sy,ry,amplitude=1.0,npml=10):
        super(Lapl2d, self).__init__()
        self.nx, self.ny = nx,ny
        self.h = h
        # pml pad
        self.npml=npml
        self.nxp = self.nx+2*npml
        self.nyp = self.ny+npml
        self.nxyp = self.nxp*self.nyp

        self.isy = int(sy/h) # source y position
        self.iry = int(ry/h) # receiver y position
        self.amplitude = amplitude # source amplitude
        self.dtype = np.float64

        self.solver = PardisoSolver(mtype=11, dtype=self.dtype)
        self.modeling = LaplOperator.apply

    def factorize(self, s, vel):
        self.s = -np.abs(s) # negative damping
        self.vel = vel
        #vmin=1.5
        #vmax=4.5
        #vp = np.clip(vel.data.numpy(),vmin,vmax)
        vp = vel.data.numpy()
        mat = impedance_matrix_vpad(self.s, vp, self.h, self.npml)
        self.solver.analyze(mat)
        self.solver.factorize()

    def forward(self, sxs):
        return self.modeling(self.vel, (self, sxs))

    def finalize(self):
        self.solver.clear()


class LaplOperator(torch.autograd.Function):
    @staticmethod
    def forward(ctx, vel, args):
        # nrhs: batch size
        # vel: (nx,ny)
        # u: (nrhs,nx,ny)
        # frd: (nrhs,nx)
        # virt: (nrhs,nx,ny)
        model, sxs = args # input: source x position (sy, ry are saved in model)

        ##u = torch.tensor(model.solve_impulse(sxs))
        nrhs = len(sxs)
        f = np.zeros((nrhs,model.nxyp),dtype=model.dtype)
        isxs = (sxs/model.h).int()
        for ishot,isx in enumerate(isxs):
            f[ishot, (isx+model.npml)*model.nyp + model.isy] = model.amplitude
        u = model.solver.solve(f)
        u.shape = (nrhs,model.nxp,model.nyp)
        u = torch.tensor( u[:,model.npml:-model.npml,:-model.npml] )

        ##
        frd = u[:,:,model.iry]
        virt = 2*model.s**2/vel**3 * u
        # save for gradient calculation
        ctx.model = model
        ctx.save_for_backward(virt)
        return frd

    @staticmethod
    def backward(ctx, grad_output):
        # resid = grad_output: (nrhs,nx)
        # b: (nrhs,nx,ny)
        virt, = ctx.saved_tensors
        model = ctx.model

        resid = grad_output

        ##b = torch.tensor(model.solve_resid(resid))
        nrhs = resid.shape[0]
        f = np.zeros((nrhs, model.nxp,model.nyp),dtype=model.dtype)
        f[:,model.npml:-model.npml,model.iry] = resid[:,:]
        f.shape=(nrhs,model.nxyp)
        b = model.solver.solve_transposed(f)
        b.shape = (nrhs,model.nxp,model.nyp)
        b = torch.tensor( b[:,model.npml:-model.npml,:-model.npml] )

        ##
        grad_input = torch.sum(virt*b, dim=0)
        return grad_input, None


torch_zero = torch.tensor(0.,dtype=torch.float64)

class LogResid(torch.autograd.Function):
    @staticmethod
    def forward(ctx, frd, true):
        #nrhs, nx = true.shape
        tolmin=1.e-80
        resid = torch.where(torch.abs(true)>tolmin, torch.log(torch.abs(frd/true)), torch_zero)

        ctx.save_for_backward(frd)
        return resid

    @staticmethod
    def backward(ctx, grad_output):
        frd, = ctx.saved_tensors
        tolmin=1.e-80
        grad_input = torch.where(torch.abs(frd)>tolmin, grad_output/frd, torch_zero)
        return grad_input, None


class SourceEstimationLogLapl():
    def __init__(self):
        self.amp = 1.0
        self.zero()

    def zero(self):
        self.isum = torch.tensor(0)
        self.sum_amp = torch.tensor(0.,dtype=torch.float64)

    def add(self, resid):
        self.sum_amp += torch.sum(resid).item()
        self.isum += torch.sum(resid != 0).item()

    def step(self):
        log_amp = np.log(self.amp)
        log_amp -= self.sum_amp/self.isum
        self.amp = np.exp(log_amp)
        self.zero()

    def amplitude(self):
        return self.amp


class Dataset(torch.utils.data.Dataset):
    def __init__(self, ftrue, sxy_all):
        self.sxy_all = sxy_all
        self.nshot = len(sxy_all)
        truedata = torch.tensor(np.fromfile(ftrue,dtype=np.float64))
        self.truedata = truedata.view(self.nshot,-1)

    def __len__(self):
        return self.nshot

    def __getitem__(self,index):
        return self.sxy_all[index], self.truedata[index]


class AllFreqDataset(torch.utils.data.Dataset):
    # num_samples=nfreq, batch size = 1: 1 batch = 1 freq, num_batch = nfreq
    def __init__(self, sxwgt, ftrue, nfreq):
        self.sxwgt = sxwgt
        self.nshot = len(sxwgt)
        self.nfreq = nfreq
        truedata = [torch.tensor(np.fromfile("%s%03d"%(ftrue,ifreq),dtype=np.float64)).view(1,self.nshot,-1)
                    for ifreq in range(nfreq)]
        self.allfreq_truedata = torch.cat(truedata,dim=0)

    def __len__(self):
        return self.nfreq

    def __getitem__(self,index):
        return self.sxwgt, self.allfreq_truedata[index]


class OneFreqDataset(torch.utils.data.Dataset):
    # num_samples=nshot, batch size=nrhs
    def __init__(self, sxwgt, truedata):
        self.sxwgt = sxwgt
        self.nshot = len(sxwgt)
        self.truedata = truedata

    def __len__(self):
        return self.nshot

    def __getitem__(self,index):
        return self.sxwgt[index], self.truedata[index]

