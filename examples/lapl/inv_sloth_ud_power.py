import torch
import numpy as np
from pathlib import Path
from LaplOperator import Lapl2d,SourceEstimationLogLapl
from LaplOperator import LogResid, Dataset
from DeconResid import ResidUDpower
from modules.io import rsf, Logger
from vel_gen import SlothGenerator

# read info - src, initial model, damp

intpldir = "./salt_timefrd/"

logger=Logger("log_sloth_ud.txt",'inv2d')

ftrue = intpldir+'/intpl/laplace.'
out_dir = 'output_sloth_ud'

Path(out_dir).mkdir(exist_ok=True)
nfreq, damps = rsf.fromfile(intpldir+'/damp.rsf','n1 data')
ny,nx,h,vinit = rsf.fromfile('data/init_salt.rsf','n1 n2 d1 data')
nshot, sxywgt = rsf.fromfile(intpldir+'/intpl/info_shot_weight.rsf','n2 data')
to_km = 0.001
sx_all = sxywgt[:,0]*to_km
sy = sxywgt[0,1]*to_km

logger.print("nfreq=%d"%nfreq)
logger.print("nshot=%d"%nshot)
logger.print("nx=%d, ny=%d"%(nx,ny))

nrhs=48

ry=sy

num_epoch = 1000

vmin,vmax=1.5,4.5
velgen = SlothGenerator(torch.tensor(vinit.copy()),vmin,vmax)
#vel = torch.tensor(vinit.copy())

lr = 0.001
optimizer = torch.optim.Adam(velgen.parameters(), lr=lr)
src_estm = [SourceEstimationLogLapl() for s in damps]
dataset = [Dataset("%s%03d"%(ftrue,ifreq),sx_all) for ifreq,_ in enumerate(damps)]
log_resid = LogResid.apply
decon_resid = ResidUDpower.apply

logger.print("initial source estimation")
for ifreq,(s,source) in enumerate(zip(damps,src_estm)):
    model = Lapl2d(nx,ny,h,sy,ry,amplitude=source.amplitude())
    model.factorize(s,velgen())
    dataloader = torch.utils.data.DataLoader(dataset[ifreq],batch_size=nrhs)
    for sxs,true in dataloader:
        frd = model(sxs)
        source.add(log_resid(frd,true))
    source.step()
logger.print("end initial source estimation")

for itr in range(num_epoch+1):

    # frequency loop
    total_loss = 0.
    optimizer.zero_grad()

    for ifreq,(s,source) in enumerate(zip(damps,src_estm)):
        #print(itr,ifreq,source.amplitude())
        model = Lapl2d(nx,ny,h,sy,ry,amplitude=source.amplitude())
        model.factorize(s,velgen())
        dataloader = torch.utils.data.DataLoader(dataset[ifreq],batch_size=nrhs)

        loss = 0.
        # shot loop
        for sxs,true in dataloader:
            frd = model(sxs)

            #print(frd.min(),frd.max(),true.min(),true.max())
            tval=10 + itr/num_epoch*100
            p=0.1
            tolmin=1.e-100
            resid = decon_resid(frd, true, p, tolmin)
            #print(resid.min(),resid.max())

            loss1 = 0.5*torch.sum(resid**2)
            total_loss += loss1.item()
            loss += loss1
            #print('loss =',loss.item())

            source.add(log_resid(frd,true))

        loss.backward()
        #print('ing loss =',total_loss)

        source.step()
        model.finalize()

    # all freq grad
    optimizer.step()


    with open("%s/source.%04d"%(out_dir,itr),'w') as fs:
        for ifreq,src in enumerate(src_estm):
            fs.write("%d %s\n"%(ifreq,src.amplitude()))

    with open("%s/error.%04d"%(out_dir,itr),'w') as fe:
        fe.write("%d %.4f\n"%(itr,total_loss))

    grad=velgen.gradient().detach().numpy()
    v = velgen().detach().numpy()
    p = velgen.par.detach().numpy()
    logger.print("itr%4d: loss=%.4f, vrange(%4.2f,%4.2f), grange(%s,%s)"%(itr, total_loss,
        v.min(), v.max(),
        grad.min(),grad.max()))

    grad.tofile("%s/ing_grad"%(out_dir))
    v.tofile("%s/ing_vel"%(out_dir))

    if itr <= 10 or itr % 10 == 0:
        grad.tofile("%s/grad.%04d"%(out_dir,itr))
        v.tofile("%s/vel.%04d"%(out_dir,itr))

