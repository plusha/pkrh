import torch
import horovod.torch as hvd
import sys
from pathlib import Path
from torchwi import Time2dForward
from torchwi.io import rsf, CFGParser
from torchwi.io import time_forward_distributed_dataloader
from pkrh.io import su, Logger

try:
    fcfg=sys.argv[1]
except:
    fcfg='par_at2d.ini'

hvd.init()

pdict = CFGParser(fcfg).parse('Time-domain Modeling')

nt,dt,w = rsf.fromfile(pdict['fwav'],"n1 d1 data")
ny,nx,h,vel,xmin = rsf.fromfile(pdict['fvel'],"n1 n2 d1 data o2")
nshot,sxy = rsf.fromfile(pdict['fshot'],"n2 data")

if pdict['limit_receiver']:
    nrcv = int(abs(pdict['max_offset']-pdict['min_offset'])/h)
else:
    nrcv = nx

if hvd.rank()==0:
    logger = Logger('log_forward.txt','td2d')
    Path(pdict['outdir']).mkdir(exist_ok=True)
    logger.print("npe=%s"%hvd.size())
    logger.print("nt=%s, dt=%s"%(nt,dt))
    logger.print("ny=%s, nx=%s, h=%s, xmin=%s"%(ny,nx,h,xmin))
    logger.print("nshot=%s"%nshot)
    logger.print("order=%d"%pdict['order'])
    logger.print("nrcv=%d"%nrcv)

DEVICE=pdict['device']

if DEVICE=='cuda' and torch.cuda.is_available():
    torch.cuda.set_device(hvd.local_rank())

vp=torch.from_numpy(vel).to(DEVICE)
w=torch.from_numpy(w)
sxy=torch.from_numpy(sxy).to(DEVICE) # source x, y coordinate

dataloader=time_forward_distributed_dataloader(sxy)

model = Time2dForward(nx,ny,h,w,dt,pdict['order'],DEVICE)
hvd.broadcast_parameters(model.state_dict(), root_rank=0)

with torch.no_grad():
    for sx,sy, ishot in dataloader:
        sx0 = sx-xmin
        ry = sy

        if pdict['limit_receiver']:
            rxmin = min(sx+pdict['min_offset'],sx+pdict['max_offset'])
            rx0= rxmin-xmin
            ir0 = int(rx0/h)
            ir1 = ir0+nrcv
        else:
            rxmin = 0.
            ir0 = 0
            ir1 = nx

        msg = "rank=%d, ishot=%d, sx=%s, isx=%d, ircv=[%d-%d]"%(hvd.rank(),ishot,sx,int(sx0/h),ir0,ir1)
        if hvd.rank()==0:
            logger.print(msg)
        else:
            print(msg)

        seismo = model(vp, sx0,sy,ry)[:,ir0:ir1].to('cpu').detach().numpy().T

        if pdict['write_su']:
            to_meter=1000.
            to_microsec=1.e6
            scalco=pdict['scalco']
            gy = ry
            with su.output(nt,"%s%04d.su"%(pdict['fseismo'],ishot)) as sf:
                for itrc,trace in enumerate(seismo):
                    gx = rxmin + itrc*h
                    trc = su.trace(ns=nt, dt=int(dt*to_microsec), data=trace,
                            fldr=ishot+1, tracf=itrc+1, scalco=scalco,
                            sx=su.scale_write(sx*to_meter,scalco),
                            sy=su.scale_write(sy*to_meter,scalco),
                            gx=su.scale_write(gx*to_meter,scalco),
                            gy=su.scale_write(gy*to_meter,scalco))
                    sf.write(trc)
        else:
            seismo.tofile("%s%04d"%(pdict['fseismo'],ishot))

if hvd.rank()==0:
    logger.print("End")
