# source wavelet
gen_wavelet nt=10001 fmax=20. dt=0.001 wav=fdgaus out=sourcewavelet.rsf

# shot geometry
gen_shot nshot=96 sx0=0.16 dsx=0.16 sz=0. out=shot.rsf

mkdir output
